#! /usr/bin/env python
import copy
import difflib
import json
import maddautils.flock as flock
import sys

try:
    import yaml
except ImportError:
    pass


class BookInventory:
    def __init__(self, default_format='yaml'):
        self.inventory = {}
        self.collisions = {}
        self.author_lookup = set()
        self.location_lookup = set()
        self.title_lookup = set()
        self.deleted_items = []
        # Don't let yaml be the default format if we can't parse it
        if 'yaml' in sys.modules:
            # yaml is available, set the desired format
            self.default_format = default_format
        else:
            self.default_format = 'json'

    def load_data(self,
                  filename='books.inventory',
                  file_format=''):
        if file_format == '':
            file_format = self.default_format

        try:
            data_handle = open(filename, 'r')
            data = data_handle.read()
            data_handle.close()

            if file_format == 'json':
                data = json.loads(data)
            elif file_format == 'yaml':
                data = yaml.safe_load(data)

            self.inventory = data

            self.update_authors()
            self.update_locations()
            self.update_titles()

            return True
        except IOError:
            # The file probably doesn't exist yet- skip it
            return False

    def update_authors(self):
        self.author_lookup = set()

        for item in self.inventory.values():
            for author in item['authors']:
                self.author_lookup.add(author)

    def update_locations(self):
        self.location_lookup = set()

        for item in self.inventory.values():
            self.location_lookup.add(item['location'])

    def update_titles(self):
        self.title_lookup = set()

        for item in self.inventory.values():
            self.title_lookup.add(item['title'])

    def find_duplicates(self, duplicates_check={}):
        """
            Check the inventory for probable duplicates.
            Can also check a provided dictionary of books.
        """
        if len(duplicates_check) == 0:
            duplicates_check = copy.deepcopy(self.inventory)

        duplicates = []
        for uid, book in duplicates_check.items():
            for d_uid, d_book in self.inventory.items():
                if uid != d_uid:
                    if self.is_same_book(
                        (uid, book,),
                        (d_uid, d_book,),
                        test_attributes=['title', 'authors']
                    ):
                        duplicates.append((d_uid, d_book,))
        return duplicates

    def is_same_book(self,
                     book1,
                     book2,
                     test_attributes=['uid', 'authors', 'title', 'location']):
        # For testing for deleted items we must check unique ID
        if 'uid' in test_attributes:
            if book1[0] != book2[0]:
                return False
            test_attributes.remove('uid')

        # Check the key properties are the same
        for key in test_attributes:
            if book1[1][key] != book2[1][key]:
                return False

        # If it is not a different book, it must be the same
        return True

    def save_data(self,
                  filename='books.inventory',
                  file_format=''):
        if file_format == '':
            file_format = self.default_format

        # Save the current copy as a temporary copy
        new_inventory = copy.deepcopy(self.inventory)

        # Lock the existing file then load it
        with flock.FileLock(filename) as lock:
            self.load_data(filename)

            # Zero the collisions list
            self.collisions = {}

            # First, attempt to resolve all deleted items
            resolved_deletions = []
            for item in self.deleted_items:
                if item[0] in self.inventory:
                    if self.is_same_book(item,
                                         [item[0], self.inventory[item[0]]]):
                        self.inventory.pop(item[0])
                        resolved_deletions.append(item[0])

            # Clean up deletions list
            self.deleted_items = [item for item in self.deleted_items
                                  if item[0] not in resolved_deletions]

            # Determine whether we can merge the dictionaries
            for key in new_inventory.keys():
                if key not in self.inventory.keys():
                    self.inventory[key] = new_inventory[key]
                elif self.inventory[key] != new_inventory[key]:
                    self.collisions[key] = new_inventory[key]

            if len(self.collisions) > 0:
                # There were collisions, fall back
                self.inventory = copy.deepcopy(new_inventory)
                lock.release()
                return False

            if len(self.inventory) > 0:
                # We successfully merged the files, save them
                if file_format == 'json':
                    data = json.dumps(self.inventory)
                elif file_format == 'yaml':
                    data = yaml.safe_dump(self.inventory)
                data_handle = open(filename, 'w')
                data_handle.write(data)
                data_handle.close()
                return True

        # File lock could not be achieved. Do not save.
        return False

    def get_next_id(self):
        if len(self.inventory) == 0:
            return 0

        ids = self.inventory.keys()
        ids = [int(x) for x in ids]
        return max(ids) + 1

    def add_book(self, title, location, authors, uid=''):
        if uid == '':
            next_id = str(self.get_next_id())
        else:
            try:
                # Check specified uid is an integer
                int(uid)
                next_id = uid
            except ValueError:
                raise ValueError('UID must be numeric. %s is not.' % uid)

            if next_id in self.inventory.keys():
                raise KeyError('UID %s already present.' % uid)

        self.inventory[next_id] = {
            'title': title,
            'location': location,
            'authors': authors
        }

    def delete_book(self, uid):
        try:
            self.deleted_items.append([uid, self.inventory[uid]])
            self.inventory.pop(uid)
        except KeyError:
            raise KeyError('Could not delete UID: %s. Not found.' % uid)

    def search_by_author(self, author):
        books = []

        for uid, item in self.inventory.items():
            item_authors = [item_author.lower()
                            for item_author
                            in item['authors']]
            if author.lower() in item_authors:
                books.append((uid, item))

        return books

    def search_similar_authors(self, author):
        authors = difflib.get_close_matches(author, self.author_lookup)
        return authors

    def search_by_title(self, title):
        books = []

        for uid, item in self.inventory.items():
            if title.lower() == item['title'].lower():
                books.append((uid, item))

        return books

    def search_similar_titles(self, title):
        books = []
        similar_titles = difflib.get_close_matches(title, self.title_lookup)
        for uid, item in self.inventory.items():
            if item['title'] in similar_titles:
                books.append((uid, item))
        return books

    def search_similar_locations(self, location):
        locations = difflib.get_close_matches(location, self.location_lookup)
        return locations

    def amend_book(self, uid, title='', new_uid='', location='', *authors):
        if uid not in self.inventory:
            raise KeyError('Could not amend UID: %s. Not found.' % uid)

        if new_uid == '':
            new_uid = uid
            if new_uid in self.inventory.keys():
                raise KeyError('UID %s already present.' % uid)

        if title == '':
            title = self.inventory[uid]['title']

        if location == '':
            location = self.inventory[uid]['location']

        if len(authors) == 0:
            authors = self.inventory[uid]['authors']

        self.delete_book(uid)

        self.inventory[uid] = {
            'title': title,
            'location': location,
            'authors': authors
        }

        return True
