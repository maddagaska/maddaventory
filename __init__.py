from maddaventory.Books import BookInventory

__author__ = 'Maddagaska'
__versioninfo__ = (0, 3, 0)
__version__ = '.'.join(map(str, __versioninfo__))

__all__ = [
    'Books'
    ]
